import numpy as np
import matplotlib.pyplot as plt

def fft_coeff(x_nodes, y_nodes):
    N = len(x_nodes)
    if N == 1:
        return y_nodes
    else:
        y_f = np.zeros_like(y_nodes, dtype=np.complex128)
        kk = np.arange(0, N//2)
        #kk = np.arange(-N//2, 0)
        E = fft_coeff(x_nodes[::2], y_nodes[::2])
        O = fft_coeff(x_nodes[1::2], y_nodes[1::2])
        y_f[:N//2] = E + np.exp(-2j*np.pi*kk / N) * O
        y_f[N//2:] = E - np.exp(-2j*np.pi*kk / N) * O
        return y_f

def trigonometric_interpolant(x, coeffs):
    y = np.real(coeffs[0])
    for i in range(1, len(coeffs)//2):
        y += 2 * np.real(coeffs[i]) * np.cos(i*x) - 2 * np.imag(coeffs[i]) * np.sin(i*x)
    return y

# Test case -- cos(x) + sin(5x) + cos(9x)
xx = np.linspace(0, 2*np.pi, 16, endpoint=False)
xxx = np.linspace(0, 2*np.pi, 200, endpoint=False)
#yy = np.cos(xx) + np.sin(5*xx) + np.cos(9*xx)
y = lambda x: 1 + 4*np.cos(x) + 5*np.sin(x) + 8*np.sin(3*x)
trig_v = np.vectorize(trigonometric_interpolant, excluded=set((1,)))
coeff = 1/len(xx) * fft_coeff(xx, y(xx))
plt.plot(xxx, y(xxx))
plt.plot(xxx, trig_v(xxx, coeff))
plt.show()

# Super-exponential convergence
y = lambda x: np.exp(np.sin(x))
xx = np.linspace(0, 2*np.pi, 4, endpoint=False)
coeff = 1/len(xx) * fft_coeff(xx, y(xx))
plt.semilogy(xxx, np.abs(trig_v(xxx, coeff) - y(xxx)))
xx = np.linspace(0, 2*np.pi, 8, endpoint=False)
coeff = 1/len(xx) * fft_coeff(xx, y(xx))
plt.semilogy(xxx, np.abs(trig_v(xxx, coeff) - y(xxx)))
xx = np.linspace(0, 2*np.pi, 16, endpoint=False)
coeff = 1/len(xx) * fft_coeff(xx, y(xx))
plt.semilogy(xxx, np.abs(trig_v(xxx, coeff) - y(xxx)))
plt.show()

# Gibbs phenomenon
xx = np.linspace(0, 2*np.pi, 128, endpoint=False)
y = np.where(xx < np.pi, -1, 1)
coeff = 1/len(xx) * fft_coeff(xx, y)
plt.plot(xxx, np.where(xxx < np.pi, -1, 1))
plt.plot(xxx, trig_v(xxx, coeff))
plt.show()

#print(1/len(xx) * fft_coeff(xx, y(xx)))