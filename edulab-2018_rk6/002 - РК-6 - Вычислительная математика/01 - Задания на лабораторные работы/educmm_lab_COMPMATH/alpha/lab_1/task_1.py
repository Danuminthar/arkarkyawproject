from decimal import *
from math import copysign, sqrt
import numpy as np
import matplotlib.pyplot as plt

ctx = getcontext()
ctx.prec = 4

def quadratic_roots_exact(a, b, c):
    D_ = b*b - 4*a*c
    return (-b + sqrt(D_))/(2*a), (-b - sqrt(D_))/(2*a)

def quadratic_roots(a, b, c):
    D_ = b*b - 4*a*c
    #return (-b + ctx.sqrt(D_))/(2*a), (-b - ctx.sqrt(D_))/(2*a)
    return (-b + ctx.sqrt(D_))/(2*a), (-b - ctx.sqrt(D_))/(2*a)

def quadratic_roots_correct(a, b, c):
    D_ = b*b - 4*a*c
    sign = lambda x: 1 if x >= 0 else -1
    q = -Decimal(0.5) * (b + sign(b) * ctx.sqrt(D_))
    return c / q, q / a

def rel_error(x_exact, x_approx):
    return np.abs(x_exact - x_approx) / np.abs(x_exact)

a_array = np.linspace(0.00001, 0.001, 100)
b = 4
c = 2
b_ = Decimal(b)
c_ = Decimal(c)

x_1_exact = np.zeros_like(a_array)
x_1_bad_approx = np.zeros_like(a_array)
x_1_good_approx = np.zeros_like(a_array)
x_2_exact = np.zeros_like(a_array)
x_2_bad_approx = np.zeros_like(a_array)
x_2_good_approx = np.zeros_like(a_array)
for i in range(a_array.shape[0]):
    a = a_array[i]
    a_ = Decimal(a)
    x_1_exact_, x_2_exact_ = quadratic_roots_exact(a, b, c)
    x_1_bad_approx_, x_2_bad_approx_ = quadratic_roots(a_, b_, c_)
    x_1_good_approx_, x_2_good_approx_ = quadratic_roots_correct(a_, b_, c_)
    x_1_exact[i] = x_1_exact_
    x_1_bad_approx[i] = float(x_1_bad_approx_)
    x_1_good_approx[i] = float(x_1_good_approx_)
    x_2_exact[i] = x_2_exact_
    x_2_bad_approx[i] = float(x_2_bad_approx_)
    x_2_good_approx[i] = float(x_2_good_approx_)

print(rel_error(x_1_exact, x_1_bad_approx))
plt.plot(a_array, rel_error(x_1_exact, x_1_bad_approx), label='x1, bad')
#plt.plot(a_array, rel_error(x_1_exact, x_1_good_approx), label='x1, good')
#plt.plot(a_array, rel_error(x_2_exact, x_2_bad_approx), label='x2, bad')
#plt.plot(a_array, rel_error(x_2_exact, x_2_good_approx), label='x2, good')
plt.grid()
plt.legend()
plt.show()