import numpy as np
import matplotlib.pyplot as plt

def l_i(i, x, x_nodes):
    x_i = x_nodes[i]
    x_diffs = x - x_nodes
    x_i_diffs = x_i - x_nodes
    return np.prod(x_diffs[:i]) * np.prod(x_diffs[i+1:]) / (np.prod(x_i_diffs[:i]) * np.prod(x_i_diffs[i+1:]))

def L(x, x_nodes, y_nodes):
    l_i_vectorized = np.vectorize(l_i, excluded=set((2,)))
    return np.dot(y_nodes, l_i_vectorized(range(len(x_nodes)), x, x_nodes))

def cheby_nodes(N):
    x_nodes_cheby = np.zeros((N,))
    x_nodes_cheby[1:-1] = np.cos((2.*np.arange(1, N-1) - 1) / (2*(N - 2)) * np.pi)
    x_nodes_cheby[0] = 1
    x_nodes_cheby[-1] = -1
    return x_nodes_cheby[::-1]

f = lambda x: 1. / (1 + 25 * x**2)
xx = np.linspace(-1, 1, 500)
plt.plot(xx, f(xx), label='$f(x)$')
L_vectorized = np.vectorize(L, excluded=set((1, 2)))

# Even mesh
N = 4
x_nodes_even = np.linspace(-1, 1, N)
y_nodes_even = f(x_nodes_even)
plt.plot(xx, L_vectorized(xx, x_nodes_even, y_nodes_even), label='$L(x)$, even')

# Chebyshev nodes
x_nodes_cheby = cheby_nodes(N)
y_nodes_cheby = f(x_nodes_cheby)
plt.plot(xx, L_vectorized(xx, x_nodes_cheby, y_nodes_cheby), label='$L(x)$, Chebyshev')
plt.grid()
plt.legend()
plt.show()

# Infinity norm
plt.clf()
Ns = np.arange(1, 18)
inf_norms_even = np.zeros_like(Ns, dtype=np.float32)
inf_norms_cheby = np.zeros_like(Ns, dtype=np.float32)
for i, N in enumerate(Ns):
    print(N)
    x_nodes_even = np.linspace(-1, 1, N)
    y_nodes_even = f(x_nodes_even)
    x_nodes_cheby = cheby_nodes(N)
    y_nodes_cheby = f(x_nodes_cheby)
    inf_norms_even[i] = max(np.abs(f(xx) - L_vectorized(xx, x_nodes_even, y_nodes_even)))
    inf_norms_cheby[i] = max(np.abs(f(xx) - L_vectorized(xx, x_nodes_cheby, y_nodes_cheby)))

plt.plot(Ns, inf_norms_even, 'o-', label='$||f(x) - L_N(x)||_{\infty}$, even')
plt.plot(Ns, inf_norms_cheby, 'o-', label='$||f(x) - L_N(x)||_{\infty}$, Chebyshev')
plt.grid()
plt.legend()
plt.show()
