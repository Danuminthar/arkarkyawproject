import numpy as np
import matplotlib.pyplot as plt

#def divided_diff(x_nodes, y_nodes):
#    x_diffs = (x_nodes - np.roll(x_nodes, 1))[1:]
#    y_diffs = (y_nodes - np.roll(y_nodes, 1))[1:]
#    print(x_nodes)
#    print(y_nodes)
#    print(x_diffs)
#    print(y_diffs)
#    return y_diffs / x_diffs
#
#def all_divided_diffs(x_nodes, y_nodes):
#    diffs_layers_number = len(x_nodes)
#    div_diffs = y_nodes
#    graph = [div_diffs]
#    for i in range(1, diffs_layers_number):
#        div_diffs = divided_diff(x_nodes[::i], div_diffs)
#        graph.append(div_diffs)
#    return graph
 
def all_divided_diffs(x_nodes, y_nodes):
    diffs_layers_number = len(x_nodes)
    div_diffs = y_nodes
    graph = [div_diffs]
    for i in range(1, diffs_layers_number):
        x_diffs = (x_nodes - np.roll(x_nodes, i))[i:]
        y_diffs = (graph[-1] - np.roll(graph[-1], 1))[1:]
        graph.append(y_diffs / x_diffs)
    return graph

def newton_forward(x, x_nodes, graph):
    final_sum_ = graph[0][0]
    x_diffs_product = 1.
    for i in range(len(x_nodes) - 1):
        x_diffs_product *= x - x_nodes[i]
        final_sum_ += graph[i + 1][0] * x_diffs_product
    return final_sum_

def newton_backward(x, x_nodes, graph):
    final_sum_ = graph[0][-1]
    x_diffs_product = 1.
    for i in range(len(x_nodes)- 1):
        x_diffs_product *= x - x_nodes[-i - 1]
        final_sum_ += graph[i + 1][-1] * x_diffs_product
    return final_sum_

def stirling_formula(x, x_nodes, graph):
    x_0_i = int(len(x_nodes) // 2)
    x_0 = x_nodes[x_0_i]
    h = x_nodes[1] - x_nodes[0]
    s = (x - x_0) / h
    final_sum_ = graph[0][x_0_i]
    print(final_sum_)
    cum_product = 1.
    for i in range(len(graph) - 1):
        diff_mid_i = int(len(graph[i + 1]) // 2)
        print('diff_mid_i: {}'.format(diff_mid_i))
        cum_product *= h
        if i % 2 == 0:
            m = i // 2
            if i != 0:
                cum_product *= s**2 - m**2
            print('i-1: {}'.format(graph[i + 1][diff_mid_i - 1]))
            print('i+1: {}'.format(graph[i + 1][diff_mid_i]))
            final_sum_ += 1./2 * s * cum_product * (graph[i + 1][diff_mid_i - 1] + graph[i + 1][diff_mid_i])
        else:
            print('i: {}'.format(graph[i + 1][diff_mid_i]))
            final_sum_ += s**2 * cum_product * graph[i + 1][diff_mid_i]
        print(final_sum_)
    return final_sum_

#f = lambda x: 1. / (1 + 25 * x**2)
f = lambda x: np.sin(x * 5)
#f = lambda x: np.exp(x/2)
print(1./(2**11))
xx = np.linspace(-1, 1, 102)
#plt.plot(xx, f(xx), label='$f(x)$')
Nf_vectorized = np.vectorize(newton_forward, excluded=set((1, 2)))
Nb_vectorized = np.vectorize(newton_backward, excluded=set((1, 2)))
S_vectorized = np.vectorize(stirling_formula, excluded=set((1, 2)))

# Even mesh
N = 101
x_nodes_even = np.linspace(-1, 1, N)
print(x_nodes_even)
y_nodes_even = f(x_nodes_even)
graph = all_divided_diffs(x_nodes_even, y_nodes_even)
print(graph)
#stirling_formula(0.5, x_nodes_even, graph)
#plt.plot(xx, Nf_vectorized(xx, x_nodes_even, graph), label='Newton forward')
#plt.plot(xx, Nb_vectorized(xx, x_nodes_even, graph), label='Newton backward')
#plt.plot(xx, S_vectorized(xx, x_nodes_even, graph), label='Stirling')
nf_errors = np.abs(Nf_vectorized(xx, x_nodes_even, graph) - f(xx)) / np.abs(f(xx))
nb_errors = np.abs(Nb_vectorized(xx, x_nodes_even, graph) - f(xx)) / np.abs(f(xx))
plt.plot(xx, nf_errors, 'o-', label='Newton forward errors')
plt.plot(xx, nb_errors, 'o-', label='Newton backward errors')
#plt.plot(xx, nf_errors - nb_errors, 'o-', label='Newton errors')
plt.grid()
plt.legend()
plt.show()