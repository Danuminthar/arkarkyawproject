[FormatInfo]
Type=TeXnicCenterProjectSessionInformation
Version=2

[Frame0]
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-1
MaxPos.y=-1
NormalPos.left=4
NormalPos.top=26
NormalPos.right=1016
NormalPos.bottom=460
Class=LaTeXView
Document=labs\educmm_lab_COMPMATH_ENG-Lab_4.tex

[Frame0_View0,0]
TopLine=0
Cursor=1925

[Frame1]
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-1
MaxPos.y=-1
NormalPos.left=4
NormalPos.top=26
NormalPos.right=1016
NormalPos.bottom=460
Class=BibTeXView
Document=bibliography.bib

[Frame1_View0,0]
TopLine=10338
Cursor=0

[Frame2]
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-1
MaxPos.y=-1
NormalPos.left=4
NormalPos.top=26
NormalPos.right=1016
NormalPos.bottom=460
Class=LaTeXView
Document=TLK\TLK_title.tex

[Frame2_View0,0]
TopLine=0
Cursor=712

[Frame3]
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-1
MaxPos.y=-1
NormalPos.left=4
NormalPos.top=26
NormalPos.right=1016
NormalPos.bottom=460
Class=LaTeXView
Document=doc-spec\preamble_common.tex

[Frame3_View0,0]
TopLine=0
Cursor=0

[Frame4]
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-1
MaxPos.y=-1
NormalPos.left=4
NormalPos.top=26
NormalPos.right=1016
NormalPos.bottom=460
Class=LaTeXView
Document=doc-spec\educmm_lab_COMPMATH_ENG-def.tex

[Frame4_View0,0]
TopLine=84
Cursor=3805

[Frame5]
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-1
MaxPos.y=-1
NormalPos.left=4
NormalPos.top=26
NormalPos.right=1016
NormalPos.bottom=460
Class=LaTeXView
Document=doc-spec\educmm_lab_COMPMATH_ENG-id.tex

[Frame5_View0,0]
TopLine=249
Cursor=663

[Frame6]
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-1
MaxPos.y=-1
NormalPos.left=4
NormalPos.top=26
NormalPos.right=1016
NormalPos.bottom=460
Class=LaTeXView
Document=doc-spec\educmm_lab_COMPMATH_ENGsa2_document_identification.tex

[Frame6_View0,0]
TopLine=0
Cursor=0

[Frame7]
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-1
MaxPos.y=-1
NormalPos.left=4
NormalPos.top=26
NormalPos.right=1016
NormalPos.bottom=460
Class=LaTeXView
Document=doc-spec\educmm_lab_COMPMATH_ENG-spec.def

[Frame7_View0,0]
TopLine=0
Cursor=275

[Frame8]
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-1
MaxPos.y=-1
NormalPos.left=4
NormalPos.top=26
NormalPos.right=1016
NormalPos.bottom=460
Class=LaTeXView
Document=labs\educmm_lab_COMPMATH_ENG-Lab_1.tex

[Frame8_View0,0]
TopLine=0
Cursor=7225

[Frame9]
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-1
MaxPos.y=-1
NormalPos.left=4
NormalPos.top=26
NormalPos.right=1016
NormalPos.bottom=460
Class=LaTeXView
Document=labs\educmm_lab_COMPMATH_ENG-Lab_2.tex

[Frame9_View0,0]
TopLine=181
Cursor=9973

[Frame10]
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-1
MaxPos.y=-1
NormalPos.left=4
NormalPos.top=26
NormalPos.right=1016
NormalPos.bottom=460
Class=LaTeXView
Document=labs\educmm_lab_COMPMATH_ENG-Lab_3.tex

[Frame10_View0,0]
TopLine=96
Cursor=5423

[Frame11]
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-1
MaxPos.y=-1
NormalPos.left=4
NormalPos.top=26
NormalPos.right=1016
NormalPos.bottom=460
Class=LaTeXView
Document=educmm_lab_COMPMATH_ENG.tex

[Frame11_View0,0]
TopLine=52
Cursor=948

[Frame12]
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-1
MaxPos.y=-1
NormalPos.left=4
NormalPos.top=26
NormalPos.right=1016
NormalPos.bottom=460
Class=LaTeXView
Document=educmm_lab_COMPMATH_ENG-FAQ.tex

[Frame12_View0,0]
TopLine=0
Cursor=0

[Frame13]
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-1
MaxPos.y=-1
NormalPos.left=4
NormalPos.top=26
NormalPos.right=1016
NormalPos.bottom=460
Class=LaTeXView
Document=educmm_lab_COMPMATH_ENG-Introduction.tex

[Frame13_View0,0]
TopLine=0
Cursor=95

[Frame14]
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-1
MaxPos.y=-1
NormalPos.left=4
NormalPos.top=26
NormalPos.right=1016
NormalPos.bottom=460
Class=BibTeXView
Document=bibliographadd.bib

[Frame14_View0,0]
TopLine=1425
Cursor=0

[Frame15]
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-1
MaxPos.y=-1
NormalPos.left=4
NormalPos.top=26
NormalPos.right=1016
NormalPos.bottom=460
Class=LaTeXView
Document=gcdopt_ths_PostDocThesis-05abrev.tex

[Frame15_View0,0]
TopLine=687
Cursor=61648

[Frame16]
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-1
MaxPos.y=-1
NormalPos.left=4
NormalPos.top=26
NormalPos.right=1016
NormalPos.bottom=460
Class=LaTeXView
Document=educmm_lab_COMPMATH_ENG-Labs.tex

[Frame16_View0,0]
TopLine=0
Cursor=0

[SessionInfo]
FrameCount=17
ActiveFrame=0

