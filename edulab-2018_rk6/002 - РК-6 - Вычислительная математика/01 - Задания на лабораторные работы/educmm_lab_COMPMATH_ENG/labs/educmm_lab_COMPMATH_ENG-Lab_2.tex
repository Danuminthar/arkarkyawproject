%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Knowledge requirements to perform}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
To perform laboratory work, the student must have knowledge:
%% все требования должны начинаться со слов "должен знать/уметь/владеть" (для последующего удовлетворения ФГОС)
\begin{itemize}
	\item know how to develop software in Python (recommended) or C++ at the basic level;
	\item know how to use software tools: numpy;
	\item know concepts: numerical differentiation, numerical integration, least squares method, trigonometric polynomials, fast Fourier transform;
\end{itemize}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Numerical differentiation}

Formulae of numerical differentiation are often derived by the decomposition in a Taylor series and the subsequent calculation of the values of a serie in multiple nodes. An increasing  the number of nodes leads to an increasing the order of the formula, i.e., the degree in which the differentiation step $h$ is raised in the expression for the residual term of the formula (it has the form $O(h^N)$). The order of the formula is easy to demonstrate if we construct a log - log graph of the dependence of the absolute error of the numerical differentiation at some given point on the differentiation step $h$. On the other hand, increasing the order of the formula requires more arithmetic floating point operations, which increase the computational error. In this assignment, we investigate both of these aspects of numerical differentiation.

\begin{task}[numerical differentiation]
Given the function
\begin{equation}
g(x) = x e^x,
\end{equation}
and node $x_0 = 2$.

Needs:
\begin{enumerate}
	\item Derive the central formula for the numerical differentiation of 4-th order with a residual member that approximates the first derivative at the 5 nodes \footnote{Hint: Decompose the function $f(x)$ in a Taylor series at the point $x_0$, then calculate the value of the number in the nodes $x_0 - 2h, x_0 - h, x_0 + h, x_0 + 2h$ and find undetermined coefficients $A, B, C, D, E$.}:
	\begin{equation}
	f'(x_0) \approx A f(x_0 - 2h) + B f(x_0 - h) + C f(x_0)+ D f(x_0 + h) + E f(x_0 + 2h).
	\end{equation}
	Demonstrate that the formula have a 4th order of accuracy.
	\item Write a function diff2(x\_0, h, f) which returns the value of the first derivative of the function $f$ on the basis of the central formulas of numerical differentiation 2-nd order at the point $x_0$ for the step of differentiating $h$\footnote{Hint: In Python, functions can be transferred as normal parameters to the method.}.
	\item Write a function diff4(x\_0, h, f) which returns the value of the first derivative of the function $f$ based on the central formula for the numerical differentiation of 4-th order at the point x\_0 for step differentiation of $h$.
	\item Calculate the derivative $g'(x)$ at $ x_0 = 2$ for the set of values $h \in [10^{-16}; 1]$ first using the diff2 function and then using the diff4\footnote{Hint: a uniform logarithmic grid for $h$ in Python can be constructed using the numpy function.logspace ().}. For both cases, construct log-log graphs of the dependence of the absolute error of numerical differentiation on the step of differentiation\footnote{Hint: log-log in matplotlib are constructed using the plt function.loglog ().}. For each case, answer the following questions:
	\begin{itemize}
		\item How can I see the order of the differentiation formula on the log-log chart? Formule prove it and show on a graph by analogy with the lectures.
		\item Does the order of the derived differentiation formula on the log-log graph coincide with its real order?
		\item What is the optimal differentiation step at which the absolute error is minimal? What is the reason for the existence of such a minimum? Justify your answer by referring to the log-log data of the chart.
	\end{itemize} 
	\item Compare the optimal differentiation step and the corresponding minimum achievable error for the 2nd and 4th order formulae. What do you think the difference between them is justified?
\end{enumerate}
\end{task}

\subsubsection{Numerical integration}

For the numerical integration of a function on a sufficiently large interval, composite formulae of numerical integration are often used. In the case where the integrable function is a polynomial, it seems logical to use a Gauss quadrature of a suitable order. In this task, we investigate the composite integration formulae, their computational stability and Gauss quadratures.
\begin{task}[numerical integration]
Given the function
\begin{equation}
g(x) = x^2 \sin 3x,
\end{equation}
set on the interval $x \in [0; \pi]$.

Needs:
\begin{enumerate}
	\item Write the composite\_simpson (a, b, n, f) function of numerical integration of the f function on the interval [a; b] over n nodes using the composite Simpson formula.
	\item Calculate the integral $\int_{0}^{\pi} g(x) dx$ using Simpson's composite formula for the set of values $n \in [3; 9999]$. Build a log - log graph of the absolute error of the numerical integration of the integration step. As in the previous task, explain how the resulting graph can determine the order of accuracy of the formula. Compare the order of the formula obtained by the graph with the analytical order of the accuracy of the Simpson's composite formula. Is there an optimal integration step for this formula that minimizes the achievable error? Justify your answer.
	\item By using theorems about roots of polynomials of Legendre proved in the lectures to derive the Gaussian quadrature with degree of precision 5. How many nodes are needed to use this quadrature?
	\item Write the gauss\_quad5 (f) function of the numerical integration of the f function using the Gauss quadrature of the fifth degree of accuracy.
\item Prove that the Gauss quadrature has a degree of accuracy of 5, using the following computational experiment:
	\begin{itemize}
		\item construct a sequence of polynomials $P_0(x)$, $P_1(x)$, $P_2(x)$, $P_3(x)$, $P_4(x)$, $P_5(x)$, $P_6(x)$ with degree, respectively $0, 1, 2, 3, 4, 5,$ and $6$, using randomly generated values of coefficients of polynomial\footnote{the Random numbers are generated by the Python function numpy.random.randn ().};
		\item integrate them on the interval $[0; 2]$ analytically and using the derived Gauss quadrature;
		\item calculate the absolute error and write a conclusion about the degree of accuracy of the derived quadrature;
		\item all calculations and calculated values must be in the report.
	\end{itemize}
\end{enumerate}
\end{task}

\subsubsection{Fast Fourier Transform}

%$O(\frac{1}{N^k})$

In modern applied mathematics, trigonometric polynomials and spectral methods are actively used to approximate periodic data and to solve differential equations, respectively. Their main advantage is the property of spectral convergence, that is, convergence is faster than $O(h^N)$ for any $N$. The property of spectral convergence, as well as the property of uniform convergence, which is a consequence of the Stone-Weierstrass theorem, strongly depends on the continuity class of the approximated function. One of the most common uses of trigonometric polynomials is the interpolation of the periodic function via the discrete Fourier transform, which is algorithmically implemented by the fast Fourier transform.

\begin{task}[FFT]
Given the function
\begin{eqnarray}
\begin{aligned}
f_1(x) &= 5 + 4\cos 2x + 2\sin 3x - \cos4x, \\
f_2(x) &= |x|,\\
f_3(x) &=  
\begin{cases} 
   -1 & -\pi \leq x < 0 \\
    1 & 0 \leq x \leq \pi
\end{cases}
\end{aligned}
\end{eqnarray}
defined on the interval $x \in [-\pi; \pi]$.

Needs:
\begin{enumerate}
	\item Using the Cooley-Tukey algorithm, write a function\newline fft\_coeff (y\_nodes) that computes and returns the complex coefficients of a trigonometric polynomial interpolating the nodes y\_nodes uniformly distributed over the segment $[-\pi; \pi]$.
	\item Test the correctness of the results of the function\newline fft\_coeff(y\_nodes) using FFT for the function $f_1 (x)$. Using the calculations from the lectures, explain how the returned complex coefficients (and their indices) are related to the original function.
	\item Write a trigonometric\_interpolant(x, Coffs) function that calculates the value of a trigonometric polynomial with the coefficients of coeffs at point x.
	\item Using the trigonometric\_interpolant and fft\_coeff functions, make a trigonometric interpolation of the $f_2(x)$ function for $N = 2^{\tilde n}$, where $\tilde n \in 1, \dots, 8$ and display the results as graphs. Analyze the continuity of the function $f_2 (x)$ and based on the graphs, write a conclusion about the convergence of this approximation:
		\begin{itemize}
		\item is the convergence uniform?
		\item is the convergence of the mean square?
	\end{itemize}
	\item Repeat the same steps for the $f_3(x)$ function and answer the same questions. What do you think is the reason for the differences?
\end{enumerate}
\end{task}

%\begin{algorithm}
%\caption{Алгоритм выполнения лабораторной работы}
%\label{educmm.algo}
%\begin{algorithmic}[1]
%	\item ...
%	\item ...
%\end{algorithmic}
%\end{algorithm}

%\begin{algorithm}
%\caption{Алгоритм выполнения лабораторной работы}
%\label{educmm.algo}
%\begin{algorithmic}[1]
%	\item ...
%	\item ...
%\end{algorithmic}
%\end{algorithm}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\subsubsection{Решение типового примера}

%...
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\subsubsection{Варианты заданий}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\begin{table}[htbp]
%	\centering
%		\begin{tabular}{|p{0.1\textwidth}|p{0.9\textwidth}|}
%		\hline
%			Номер & Исходные данные \\
%		\hline
%			\rownumber & ввв \\
%			\rownumber & ввв \\
%		\hline
%		\end{tabular}
%	\caption{Варианты заданий}
%	\label{tab:TasksVariants}
%\end{table}
%\setcounter{magicrownumbers}{0}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

