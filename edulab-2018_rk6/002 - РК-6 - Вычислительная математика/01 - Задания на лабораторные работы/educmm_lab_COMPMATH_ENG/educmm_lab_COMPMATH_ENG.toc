\select@language {russian}
\contentsline {section}{\numberline {1}Laboratory Work}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}Laboratory work 1}{2}{subsection.1.1}
\contentsline {subsubsection}{\numberline {1.1.1}Knowledge requirements to perform}{2}{subsubsection.1.1.1}
\contentsline {subsubsection}{\numberline {1.1.2}Interpolation of functions by Lagrange polynomials}{2}{subsubsection.1.1.2}
\contentsline {subsubsection}{\numberline {1.1.3}Local interpolation by cubic splines}{3}{subsubsection.1.1.3}
\contentsline {subsection}{\numberline {1.2}Laboratory work 2}{5}{subsection.1.2}
\contentsline {subsubsection}{\numberline {1.2.1}Knowledge requirements to perform}{5}{subsubsection.1.2.1}
\contentsline {subsubsection}{\numberline {1.2.2}Numerical differentiation}{5}{subsubsection.1.2.2}
\contentsline {subsubsection}{\numberline {1.2.3}Numerical integration}{6}{subsubsection.1.2.3}
\contentsline {subsubsection}{\numberline {1.2.4}Fast Fourier Transform}{8}{subsubsection.1.2.4}
\contentsline {subsection}{\numberline {1.3}Laboratory work 3}{10}{subsection.1.3}
\contentsline {subsubsection}{\numberline {1.3.1}Knowledge requirements to perform}{10}{subsubsection.1.3.1}
\contentsline {subsubsection}{\numberline {1.3.2}LU-decomposition}{10}{subsubsection.1.3.2}
\contentsline {subsection}{\numberline {1.4}Laboratory work 4}{12}{subsection.1.4}
\contentsline {subsubsection}{\numberline {1.4.1}Knowledge requirements to perform}{12}{subsubsection.1.4.1}
\contentsline {subsubsection}{\numberline {1.4.2}Model of Lotka\IeC {\textendash }Volterra}{12}{subsubsection.1.4.2}
\contentsline {section}{\numberline {2}Questions and Answers}{14}{section.2}
